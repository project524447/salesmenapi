# Salesmen API

This is a simple API for managing salesmen data built using Laravel.

## Requirements

- PHP 8.1
- Composer
- A database system like PostgreSQL

## Installation

Clone the repository to your local machine:

```bash
git clone https://your-gitlab-repo-link.git
```

Navigate into the project directory:

```bash
cd salesmenapi
```

Install the dependencies using Composer:

```bash
composer install
```

## Configuration

Copy the .env.example file to create a .env file:

```bash
cp .env.example .env
```

Open the .env file and update the following settings:

Set APP_DEBUG to false for the production environment.
Set APP_URL to the root URL of your API.
```env
APP_DEBUG=false
APP_URL=https://your.url/api
```

Configure the database connection settings. This example assumes you are using PostgreSQL:

```env
DB_CONNECTION=pgsql
DB_HOST=db
DB_PORT=5432
DB_DATABASE=salesmen
DB_USERNAME=user
DB_PASSWORD=password
```

Don't forget to replace http://your.url/api with the actual URL of your API and database credentials with the ones for your environment.

Generate the application key:

```bash
php artisan key:generate
```

Run the migrations to create the database schema:

```bash
php artisan migrate
```
(Optional) Seed the database with initial data:

```bash
php artisan db:seed
```

Running the Application
Start the Laravel server:

```bash
php artisan serve
```
The API will be available at the URL defined in your APP_URL setting.

## API Routes
Here are some of the available API routes:

GET /api/salesmen - Retrieve a list of salesmen.
GET /api/salesmen/{salesman_uuid} - Retrieve details of a specific salesman.
POST /api/salesmen - Create a new salesman.
PUT /api/salesmen/{salesman_uuid} - Update a specific salesman.
DELETE /api/salesmen/{salesman_uuid} - Delete a specific salesman.

Examples
Get List of Salesmen
```bash
curl -X GET "https://your.url/api/salesmen" -H "accept: application/json"
```
Get Salesman Details
Replace {salesman_uuid} with the actual UUID of the salesman.

```bash
curl -X GET "https://your.url/api/salesmen/{salesman_uuid}" -H "accept: application/json"
```
Create a New Salesman
```bash
curl -X POST "https://your.url/api/salesmen" -H "Content-Type: application/json" -d '{"name": "John Doe", "email": "jdoe@example.com"}'
```

## Contributing
Please feel free to fork this repository and contribute by submitting a pull request.

## License
This Salesmen API is open-sourced software licensed under the MIT license.
