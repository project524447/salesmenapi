<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


class SalesmanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request): array
    {
        // Pripojte tituly pred a za menom, ak existujú
        $displayName = $this->createDisplayName();

        $data = [
            'id' => $this->uuid,
            //'self' => route('salesmen.show', $this->uuid), 
            'self' => '/salesmen/' . $this->uuid,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'display_name' => $displayName,
            'titles_before' => $this->titles_before,
            'titles_after' => $this->titles_after,
            'prosight_id' => $this->prosight_id,
            'email' => $this->email,
            'phone' => $this->phone,
            'gender' => $this->gender,
            'marital_status' => $this->marital_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
        return $data;        

    }

    /**
     * Vytvorí zobrazené meno predajcu kombinovaním titulov pred a za menom s menom.
     *
     * @return string
     */
    protected function createDisplayName(): string
    {
        $titlesBefore = implode(' ', $this->titles_before ?? []);
        $titlesAfter = implode(' ', $this->titles_after ?? []);

        return trim("{$titlesBefore} {$this->first_name} {$this->last_name} {$titlesAfter}");
    }
 
}
