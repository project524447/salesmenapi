<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSalesmanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        // Autorizácia by mala byť nastavená na true, aby sme povolili používanie tejto request triedy
        // Toto môžete neskôr obmedziť podľa potreby, napr. iba pre autentifikovaných používateľov
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'sometimes|string|min:2|max:50',
            'last_name' => 'sometimes|string|min:2|max:50',
            'email' => 'sometimes|string|email|max:255',
            'phone' => 'nullable|string|max:255',
            'gender' => [
                'sometimes',
                Rule::in(['m', 'f'])
            ],
            'marital_status' => [
                'nullable', 
                Rule::exists('marital_statuses', 'code')
            ],
            'titles_before' => 'nullable|array|min:0|max:10',
            'titles_before.*' => [
                'string',
                Rule::exists('titles', 'code')
            ],
            'titles_after' => 'nullable|array|min:0|max:10',
            'titles_after.*' => [
                'string',
                Rule::exists('titles', 'code')
            ],
            //'prosight_id' => 'sometimes|string|size:5',
            'prosight_id' => [
                'sometimes',
                'string',
                'size:5',
                Rule::unique('salesmen')->ignore($this->salesman)
            ],            
        ];
    }
}
