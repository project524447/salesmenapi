<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSalesmanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        // Autorizácia by mala byť nastavená na true, aby sme povolili používanie tejto request triedy
        // Toto môžete neskôr obmedziť podľa potreby, napr. iba pre autentifikovaných používateľov
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string|min:2|max:50',
            'last_name' => 'required|string|min:2|max:50',
            //'email' => 'required|string|email|max:255|unique:salesmen,email',
            'email' => 'required|string|email|max:255',
            'phone' => 'nullable|string|max:255',
            'gender' => ['required', Rule::in(['m', 'f'])],
            'marital_status' => [
                'nullable', 
                Rule::exists('marital_statuses', 'code') // musí zodpovedať hodnotám v databáze
            ],
            'titles_before' => 'nullable|array|min:0|max:10',
            'titles_before.*' => [
                'string',
                Rule::exists('titles', 'code') //  musí zodpovedať hodnotám v databáze
            ],
            'titles_after' => 'nullable|array|min:0|max:10',
            'titles_after.*' => [
                'string',
                Rule::exists('titles', 'code') //  musí zodpovedať hodnotám v databáze
            ],
            //'prosight_id' => 'required|string|size:5|unique:salesmen,prosight_id',
            'prosight_id' => 'required|string|size:5',
        ];
    }
}
