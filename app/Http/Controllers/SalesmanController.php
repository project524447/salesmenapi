<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Salesman;
use App\Http\Resources\SalesmanResource;
use App\Http\Requests\StoreSalesmanRequest;
use App\Http\Requests\UpdateSalesmanRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;



class SalesmanController extends Controller
{
    public function index(Request $request)
    {

        try {        
            // logika pre spracovanie parametrov 'page', 'per_page' a 'sort'
            $perPage = $request->input('per_page', 10);
            $sort = $request->input('sort', 'created_at');
            
            $query = Salesman::query();
        
            // Zoradenie podľa stĺpca a smeru
            $direction = Str::startsWith($sort, '-') ? 'desc' : 'asc';
            $column = ltrim($sort, '-');

            // Ignorovanie zoradenia pre stĺpce, ktoré sú typu JSON
            $unsortableColumns = ['titles_before', 'titles_after'];

            // kontrola či je názov stĺpca platný
            if (Schema::hasColumn('salesmen', $column) && !in_array($column, $unsortableColumns)) {
                $query->orderBy($column, $direction);
            }
        
            $salesmen = $query->paginate($perPage);
            return SalesmanResource::collection($salesmen);
        } catch (\Exception $e) {
            // V prípade chyby vráti HTTP 400 s príslušným popisom
            return response()->json(['error' => 'Bad request'], 400);
        }
    }
    
    public function store(StoreSalesmanRequest $request)
    {
        $prosightId = $request->input('prosight_id');
        
        // Kontrola, či už existuje predajca s daným prosight_id
        if (Salesman::where('prosight_id', $prosightId)->exists()) {
            return response()->json([
                'errors' => [
                    [
                        'code' => 'PERSON_ALREADY_EXISTS',
                        'message' => 'Salesman with such prosight_id ' . $prosightId . ' is already registered.'
                    ]
                ]
            ], Response::HTTP_CONFLICT);
        }
    
        $salesman = Salesman::create($request->validated());
        return (new SalesmanResource($salesman))
            ->response()
            ->setStatusCode(201);
    }
   

    /*public function show($uuid)
    {
        $salesman = Salesman::findOrFail($uuid);
        return new SalesmanResource($salesman);
    }*/
    public function show($salesman_uuid)
    {
        /*$salesman = Salesman::find($salesman_uuid);
    
        if (!$salesman) {
            return response()->json([
                'errors' => [
                    [
                        'code' => 'PERSON_NOT_FOUND',
                        'message' => 'Salesman with such uuid not found. [' . "Salesman \"" . $salesman_uuid . "\" not found.]"
                    ]
                ]
            ], 404);
        }*/
        
        // Hľadanie obchodného zástupcu podľa UUID
        $salesman = Salesman::where('uuid', $salesman_uuid)->first();

        // Kontrola, či bol obchodný zástupca nájdený
        if (!$salesman) {
            return response()->json([
                'errors' => [
                    [
                        'code' => 'PERSON_NOT_FOUND',
                        'message' => "Salesman \"{$salesman_uuid}\" not found."
                    ]
                ]
            ], 404);
        }        

        //$salesman = Salesman::findOrFail($uuid);
        return new SalesmanResource($salesman);
    }    

    public function update(UpdateSalesmanRequest $request, $uuid)
    {
        $salesman = Salesman::find($uuid);
    
        if (!$salesman) {
            return response()->json([
                'errors' => [
                    [
                        'code' => 'PERSON_NOT_FOUND',
                        'message' => 'Salesman with such uuid not found. [' . "Salesman \"" . $uuid . "\" not found.]"
                    ]
                ]
            ], 404);
        }
    
        $salesman->update($request->validated());
        return new SalesmanResource($salesman);
    }

    public function destroy($uuid)
    {
        $salesman = Salesman::find($uuid);
    
        if (!$salesman) {
            return response()->json([
                'errors' => [
                    [
                        'code' => 'PERSON_NOT_FOUND',
                        'message' => 'Salesman with such uuid not found. [' . "Salesman \"" . $uuid . "\" not found.]"
                    ]
                ]
            ], 404);
        }
    
        $salesman->delete();
        return response()->json(null, 204);
    }

}
