<?php

namespace App\Http\Controllers;

use App\Models\Gender;
use App\Models\MaritalStatus;
use App\Models\Title;
use Illuminate\Http\Response;

class CodelistController extends Controller
{
    public function index()
    {
        $genders = Gender::all();
        $maritalStatuses = MaritalStatus::all();
        $titles = Title::all();

        return response()->json([
            'genders' => $genders,
            'marital_statuses' => $maritalStatuses,
            'titles' => $titles
        ], Response::HTTP_OK);
    }
}

