<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (ModelNotFoundException $e, $request) {
            if ($request->expectsJson()) {
                return response()->json([
                    'errors' => [
                        [
                            'code' => 'PERSON_NOT_FOUND',
                            'message' => 'Salesman with such uuid not found. [' . $e->getModel() . " \"" . $e->getIds()[0] . "\" not found.]"
                        ]
                    ]
                ], 404);
            }
        });

        /*$this->renderable(function (ModelNotFoundException $e, $request) {
            if ($request->expectsJson()) {
                // Získanie modelu a kľúča z výnimky
                $model = strtolower(class_basename($e->getModel()));
                $key = $e->getIds()[0]; // Predpokladá sa, že je to jednoznačný identifikátor (napr. UUID)

                return response()->json([
                    'errors' => [
                        [
                            'code' => 'PERSON_NOT_FOUND',
                            'message' => "Salesman \"{$key}\" not found."
                        ]
                    ]
                ], 404);
            }
        });*/

        // Pridanie vlastného spracovania pre ValidationException
        $this->renderable(function (ValidationException $e, $request) {
            if ($request->expectsJson()) {
                $formattedErrors = collect($e->errors())->map(function ($errors, $field) {
                    return collect($errors)->map(function ($error) use ($field) {
                        return [
                            'code' => 'INPUT_DATA_BAD_FORMAT',
                            'message' => "Bad format of input data. {$error}"
                        ];
                    });
                })->flatten(1);

                return response()->json([
                    'errors' => $formattedErrors
                ], 400); // Nastavenie HTTP stavového kódu na 400
            }
        });

        
    }
}

