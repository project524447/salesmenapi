<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Str;


class Salesman extends Model
{
    use SoftDeletes, HasFactory;

    protected $primaryKey = 'uuid'; // Nastavte názov primárneho kľúča
    public $incrementing = false; // UUID nie je auto-incrementing
    protected $keyType = 'string'; // Typ primárneho kľúča je string

    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'gender',
        'marital_status', 'titles_before', 'titles_after', 'prosight_id'
    ];

    protected $casts = [
        'titles_before' => 'array',
        'titles_after' => 'array',
    ];


    protected static function boot()
    {
        parent::boot();
        static::creating(function ($salesman) {
            if (empty($salesman->{$salesman->getKeyName()})) {
                $salesman->{$salesman->getKeyName()} = Str::uuid();
            }
        });
    }    

    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender', 'code');
    }

    public function maritalStatus()
    {
        return $this->belongsTo(MaritalStatus::class, 'marital_status', 'code');
    }

    public function createDisplayName()
    {
        $titlesBefore = is_array($this->titles_before) ? implode(' ', $this->titles_before) . ' ' : '';
        $titlesAfter = is_array($this->titles_after) ? ' ' . implode(' ', $this->titles_after) : '';
    
        return $titlesBefore . $this->first_name . ' ' . $this->last_name . $titlesAfter;
    } 

    public function getRouteKeyName()
    {
        return 'uuid'; // Použite 'uuid' namiesto 'id' pre vyhľadávanie v trasách
    }    

}
