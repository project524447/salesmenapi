<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['code', 'name'];

}
