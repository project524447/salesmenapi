<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['code', 'name'];

    public function salesmen()
    {
        return $this->hasMany(Salesman::class, 'gender', 'code');
    }
}
