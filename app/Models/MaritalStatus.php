<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaritalStatus extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['code', 'name'];

    public function salesmen()
    {
        return $this->hasMany(Salesman::class, 'marital_status', 'code');
    }
}
