<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SalesmanController;
use App\Http\Controllers\CodelistController;

use App\Models\Salesman;
use App\Http\Requests\StoreSalesmanRequest;
use App\Http\Resources\SalesmanResource;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('codelists', [CodelistController::class, 'index'])->name('codelists.index');

//Route::apiResource('salesmen', SalesmanController::class);

// Vytvorenie nového obchodného zástupcu
Route::post('/salesmen', [SalesmanController::class, 'store']);

// Získanie zoznamu obchodných zástupcov
Route::get('/salesmen', [SalesmanController::class, 'index']);

// Aktualizácia konkrétneho obchodného zástupcu
Route::put('/salesmen/{salesman_uuid}', [SalesmanController::class, 'update']);

// Získanie detailných informácií o konkrétnom obchodnom zástupcovi
Route::get('/salesmen/{salesman_uuid}', [SalesmanController::class, 'show']);

// Odstránenie konkrétneho obchodného zástupcu
Route::delete('/salesmen/{salesman_uuid}', [SalesmanController::class, 'destroy']);

// Získanie kódových zoznamov
Route::get('/codelists', [CodelistController::class, 'index']);

Route::get('/salesmen/{salesman_uuid}', [SalesmanController::class, 'show'])->name('salesmen.show');

/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/