<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API Usage Guide</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container mt-5">
        <h1 class="mb-4">Welcome to the Salesman API</h1>
        <p>This page provides a basic guide on how to use the Salesman API.</p>

        <div class="mt-4">
            <h2>API Endpoints</h2>

            <div class="mt-3">
                <h3>Get List of Salesmen</h3>
                <p><strong>Endpoint:</strong> GET /api/salesmen</p>
                <p>Returns a list of all salesmen.</p>
            </div>

            <div class="mt-3">
                <h3>Get Salesman Details</h3>
                <p><strong>Endpoint:</strong> GET /api/salesmen/{salesman_uuid}</p>
                <p>Returns detailed information about a specific salesman identified by UUID.</p>
            </div>

            <div class="mt-3">
                <h3>Create a New Salesman</h3>
                <p><strong>Endpoint:</strong> POST /api/salesmen</p>
                <p>Creates a new salesman with the provided data.</p>
            </div>

            <div class="mt-3">
                <h3>Update Salesman Information</h3>
                <p><strong>Endpoint:</strong> PUT /api/salesmen/{salesman_uuid}</p>
                <p>Updates the information of the specified salesman.</p>
            </div>

            <div class="mt-3">
                <h3>Delete a Salesman</h3>
                <p><strong>Endpoint:</strong> DELETE /api/salesmen/{salesman_uuid}</p>
                <p>Deletes the specified salesman.</p>
            </div>

            <div class="mt-3">
                <h3>Get Codelists</h3>
                <p><strong>Endpoint:</strong> GET /api/codelists</p>
                <p>Returns a list of codelists used in the application.</p>
            </div>

            <div class="mt-3">
                <h2>How to Use</h2>
                <p>To interact with the API, you can use tools like curl, Postman, or any HTTP client in your preferred programming language.</p>
                
                <div class="mt-3">
                    <h3>Example Request</h3>
                    <p>Here is an example of how to request the list of salesmen using curl:</p>
                    <pre>curl -X GET "https://salesmen.afroplants.eu/api/salesmen" -H "accept: application/json"</pre>
                </div>
                
                <!-- You can add more examples for other endpoints as necessary -->
            </div>
        </div>
    </div>

    <!-- Bootstrap JS and Popper.js -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
