# Použite PHP s Apache serverom
FROM php:8.1-apache

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Inštalácia systémových závislostí
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    libzip-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    libmagickwand-dev \
    libicu-dev \
    postgresql-client \
    libpq-dev \
    nano && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Inštalácia rozšírení PHP potrebných pre PostgreSQL
RUN docker-php-ext-install pdo_pgsql pgsql

# Inštalácia ďalších PHP rozšírení
RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg \
    && docker-php-ext-install mbstring exif pcntl bcmath gd intl

# Nastavenie pracovného adresára v kontajneri
WORKDIR /var/www/html

# Kopírovanie súborov projektu do kontajnera
COPY . /var/www/html

# Inštalácia závislostí Composeru
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN composer install

# Nastavenie oprávnení
RUN chown -R www-data:www-data /var/www/html \
    && a2enmod rewrite

# Úprava Apache konfigurácie
RUN sed -i 's|/var/www/html|/var/www/html/public|g' /etc/apache2/sites-available/000-default.conf

