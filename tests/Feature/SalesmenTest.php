<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Salesman;

class SalesmenTest extends TestCase
{
    use WithFaker, RefreshDatabase;


    public function test_create_salesman(): void
    {
        $salesmanData = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'prosight_id' => $this->faker->bothify('#####'), // Generuje 5-znakový reťazec
            'email' => $this->faker->safeEmail,
            'phone' => $this->faker->optional()->phoneNumber,
            'gender' => $this->faker->randomElement(['m', 'f']),
            'marital_status' => $this->faker->optional()->randomElement(['single', 'married', 'divorced', 'widowed']),
            'titles_before' => $this->faker->optional()->randomElements(['Bc.', 'Mgr.', 'Ing.', 'PhDr.', 'RNDr.'], $this->faker->numberBetween(0, 3)),
            'titles_after' => $this->faker->optional()->randomElements(['PhD.', 'CSc.', 'MBA', 'LL.M'], $this->faker->numberBetween(0, 3)),
            ];

        $response = $this->json('POST', '/api/salesmen', $salesmanData);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'first_name',
                'last_name',
                'prosight_id',
                'email',
                'phone',
                'gender',
                'marital_status',
                'titles_before',
                'titles_after',
                'created_at',
                'updated_at',
            ],
        ]);
    }


    public function test_list_salesmen(): void
    {
        // Vytvorenie vzorových dát obchodníkov
        Salesman::factory()->count(10)->create();

        $response = $this->get('/api/salesmen');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'first_name',
                    'last_name',
                    'prosight_id',
                    'email',
                    'phone',
                    'gender',
                    'marital_status',
                    'titles_before',
                    'titles_after',
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);
    }
    

    public function test_show_salesman(): void
    {
        $salesman = Salesman::factory()->create();
        $response = $this->get("/api/salesmen/{$salesman->uuid}");

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $salesman->uuid,
                'self' => route('salesmen.show', $salesman->uuid),
                'first_name' => $salesman->first_name,
                'last_name' => $salesman->last_name,
                'display_name' => $salesman->createDisplayName(),
                'titles_before' => $salesman->titles_before,
                'titles_after' => $salesman->titles_after,
                'prosight_id' => $salesman->prosight_id,
                'email' => $salesman->email,
                'phone' => $salesman->phone,
                'gender' => $salesman->gender,
                'marital_status' => $salesman->marital_status,
                'created_at' => $salesman->created_at->toJSON(),
                'updated_at' => $salesman->updated_at->toJSON(),
                // Prípadné ďalšie polia, ktoré chcete overiť
            ],
        ]);
    }

    public function test_delete_salesman(): void
    {
        $salesman = Salesman::factory()->create();

        $response = $this->delete("/api/salesmen/{$salesman->uuid}");

        $response->assertStatus(204); // No content
    }


}
