<?php
namespace Database\Factories;

use App\Models\Salesman;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SalesmanFactory extends Factory
{

    protected $model = Salesman::class;

    public function definition()
    {
        return [
            'uuid' => (string) Str::uuid(),
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'prosight_id' => $this->faker->unique()->numerify('#####'),
            'email' => $this->faker->unique()->safeEmail,
            'phone' => $this->faker->optional()->phoneNumber,
            'gender' => $this->faker->randomElement(['m', 'f']),
            'marital_status' => $this->faker->optional()->randomElement(['single', 'married', 'divorced', 'widowed']),
            'titles_before' => $this->faker->optional()->randomElement([['Bc.', 'Mgr.', 'Ing.', 'JUDr.']]),
            'titles_after' => $this->faker->optional()->randomElement([['PhD.', 'CSc.', 'DrSc.']]),
            'created_at' => now(),
            'updated_at' => now(),
            // 'deleted_at' => null - if using SoftDeletes, this attribute is not needed in the factory
        ];
    }
}
