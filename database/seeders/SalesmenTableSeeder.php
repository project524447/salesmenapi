<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class SalesmenTableSeeder extends Seeder
{
    public function run()
    {
        // Načítanie CSV súboru a vloženie dát
        $csvFile = base_path('data/salesmen.csv'); // Cesta k CSV súboru
        $data = array_map(function ($line) {
            return str_getcsv($line, ";"); // použitie bodkočiarky ako oddeľovača
        }, file($csvFile));
        
        $headers = array_map('trim', $data[0]); // Odstránenie medzier a trim na hlavičke
        $rows = array_slice($data, 1);

        foreach ($rows as $row) {
            $row = array_combine($headers, $row);
            //dd($row);
            DB::table('salesmen')->insert([
                'uuid' => Str::uuid(), // Vygeneruje nový UUID
                'first_name' => $row['first_name'],
                'last_name' => $row['last_name'],
                'prosight_id' => $row['prosight_id'],
                'email' => $row['email'],
                'phone' => $row['phone'] ?: null, // Použije null ak je prázdny
                'gender' => $row['gender'],
                'marital_status' => $row['marital_status'] ?: null, // Použije null ak je prázdny
                'titles_before' => json_encode(explode(',', $row['titles_before'])), // Predpokladá, že tituly sú oddelené čiarkami
                'titles_after' => json_encode(explode(',', $row['titles_after'])), // Predpokladá, že tituly sú oddelené čiarkami
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}