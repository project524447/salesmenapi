<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('genders', function (Blueprint $table) {
            //$table->id();
            $table->string('code', 1)->primary();  // 'm' alebo 'f'
            $table->string('name');
        });

        // Vloženie preddefinovaných dát
        DB::table('genders')->insert([
            ['code' => 'm', 'name' => 'muž'],
            ['code' => 'f', 'name' => 'žena']
        ]);        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('genders');
    }
};
