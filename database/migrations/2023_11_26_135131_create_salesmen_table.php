<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('salesmen', function (Blueprint $table) {
            /*$table->id();
            $table->uuid('uuid');*/
            $table->uuid('uuid')->primary();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('prosight_id', 5);  // Predpokladá sa 5-znakový reťazec
            $table->string('email');
            $table->string('phone')->nullable();
            $table->string('gender', 1);  // Predpokladá sa kód pohlavia ako 'm' alebo 'f'
            $table->string('marital_status')->nullable();
            $table->json('titles_before')->nullable();  // Uložené ako JSON
            $table->json('titles_after')->nullable();  // Uložené ako JSON
            $table->timestamps();
            $table->softDeletes();  // Pridá stĺpec deleted_at s indexom
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('salesmen');
    }
};
