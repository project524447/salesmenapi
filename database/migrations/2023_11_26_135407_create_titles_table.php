<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('titles', function (Blueprint $table) {
            //$table->id();
            $table->string('code')->primary();
            $table->string('name');
        });

       // Vloženie preddefinovaných dát pre tituly
       $titles = [
        'Bc.', 'Mgr.', 'Ing.', 'JUDr.', 'MVDr.', 'MUDr.', 'PaedDr.', 'prof.',
        'doc.', 'dipl.', 'MDDr.', 'Dr.', 'Mgr. art.', 'ThLic.', 'PhDr.',
        'PhMr.', 'RNDr.', 'ThDr.', 'RSDr.', 'arch.', 'PharmDr.', 
        'CSc.', 'DrSc.', 'PhD.', 'ArtD.', 'DiS', 'DiS.art', 'FEBO', 'MPH',
        'BSBA', 'MBA', 'DBA', 'MHA', 'FCCA', 'MSc.', 'FEBU', 'LL.M'
        ];

        foreach ($titles as $title) {
            DB::table('titles')->insert([
                'code' => $title,
                'name' => $title
            ]);
        }        
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('titles');
    }
};
